package com.rest;
// Generated Apr 8, 2014 6:32:28 AM by Hibernate Tools 3.2.1.GA


import java.util.HashSet;
import java.util.Set;

/**
 * TblDesignation generated by hbm2java
 */
public class TblDesignation  implements java.io.Serializable {


     private int desigId;
     private String desigName;
     private Set tblTstaffs = new HashSet(0);

    public TblDesignation() {
    }

	
    public TblDesignation(int desigId, String desigName) {
        this.desigId = desigId;
        this.desigName = desigName;
    }
    public TblDesignation(int desigId, String desigName, Set tblTstaffs) {
       this.desigId = desigId;
       this.desigName = desigName;
       this.tblTstaffs = tblTstaffs;
    }
   
    public int getDesigId() {
        return this.desigId;
    }
    
    public void setDesigId(int desigId) {
        this.desigId = desigId;
    }
    public String getDesigName() {
        return this.desigName;
    }
    
    public void setDesigName(String desigName) {
        this.desigName = desigName;
    }
    public Set getTblTstaffs() {
        return this.tblTstaffs;
    }
    
    public void setTblTstaffs(Set tblTstaffs) {
        this.tblTstaffs = tblTstaffs;
    }




}


