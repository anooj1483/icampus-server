/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.rest.resources;

import com.rest.TblOnline;
import com.rest.worker.LoginDbWorker;
import com.rest.worker.LogoutDbWorker;
import com.sun.xml.ws.api.SOAPVersion;
import com.sun.xml.ws.client.sei.ResponseBuilder;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.UUID;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.UriInfo;
import javax.ws.rs.PathParam;
import javax.ws.rs.Consumes;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.GET;
import javax.ws.rs.Produces;
import javax.enterprise.context.RequestScoped;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.DELETE;
import javax.ws.rs.POST;
import javax.ws.rs.core.Cookie;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.core.Response;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONTokener;

/**
 * REST Web Service
 *
 * @author phenom
 */
@Path("generic")
@RequestScoped
public class LoginResource {

    @Context
    private UriInfo context;
    private LoginDbWorker mDb;
    private String Id = "";

    /**
     * Creates a new instance of LoginResource
     */
    public LoginResource() {
    }

    /**
     * Retrieves representation of an instance of com.rest.LoginResource
     *
     * @return an instance of java.lang.String
     */
    @GET
    @Produces("application/json")
    public String getJson() {
        //TODO return proper representation object
        //throw new UnsupportedOperationException();

        mDb = new LoginDbWorker();
        return mDb.getlogin();
    }

    /**
     * PUT method for updating or creating an instance of LoginResource
     *
     * @param content representation for the resource
     * @return an HTTP response with content of the updated or created resource.
     */
    @POST
    @Consumes("application/json")
    @Produces("application/json")
    public Response postJson(String content, @Context HttpHeaders headers) throws JSONException {



        //String userAgent = headers.getRequestHeader("test").get(0);
        mDb = new LoginDbWorker();
        String checked = mDb.checkLogin(content);
        System.out.print(checked);

        if (!checked.contains("Failed")) {
            UUID idOne = UUID.randomUUID();
            String sessionid = "" + idOne;
            
            mDb=new LoginDbWorker();
            String s=mDb.SessionBuilder(sessionid, checked, content);
            if(s.contains("Success"))  
                return Response.ok(mDb.checkLogin(content)).header("x-session", sessionid).build();
            else{
                LogoutDbWorker mWorker=new LogoutDbWorker();
            
                return Response.ok(mWorker.logoutwithid(content)).header("x-session", "Login Again").build();
            }
        } else {
            LogoutDbWorker mWorker=new LogoutDbWorker();
            return Response.ok(mWorker.logoutwithid(content)).header("x-session", "Login Again").build();
        }
        //return mDb.checkLogin(content);
        //return content;

    }

    @PUT
    @Consumes("application/json")
    public void putJson(String content) {
    }
    
    
}
