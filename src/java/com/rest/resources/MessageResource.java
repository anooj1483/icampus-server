/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.rest.resources;

import com.rest.worker.MessageWorker;
import com.rest.worker.SessionChecker;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.List;
import java.util.Map;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.UriInfo;
import javax.ws.rs.PathParam;
import javax.ws.rs.Consumes;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.GET;
import javax.ws.rs.Produces;
import javax.enterprise.context.RequestScoped;
import javax.ws.rs.POST;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.core.Response;
import org.apache.commons.io.IOUtils;
import org.jboss.resteasy.plugins.providers.multipart.InputPart;
import org.jboss.resteasy.plugins.providers.multipart.MultipartFormDataInput;
import org.json.JSONException;



/**
 * REST Web Service
 *
 * @author phenom
 */
@Path("message")
@RequestScoped
public class MessageResource {

    @Context
    private UriInfo context;

    /**
     * Creates a new instance of MessageResource
     */
    public MessageResource() {
    }

    /**
     * Retrieves representation of an instance of com.rest.resources.MessageResource
     * @return an instance of java.lang.String
     */
    @GET
    @Path("/department/")
    @Produces("application/json")
    public String getJson(@Context HttpHeaders headers) {
        //TODO return proper representation object
        //throw new UnsupportedOperationException();
        String userAgent = headers.getRequestHeader("x-session").get(0);
        SessionChecker check=new SessionChecker();
        MessageWorker mWorker=new MessageWorker();
        String resp=check.Checker(userAgent);
        if(!resp.contains("Logout")){
            
        return mWorker.getDepartment();
        }
        else
            return"Logout";
    }
    
    
    
    @GET
    @Path("/noticeboard/")
    @Produces("application/json")
    public String getNoticeString(@Context HttpHeaders headers) throws JSONException {
        //TODO return proper representation object
        //throw new UnsupportedOperationException();
        String userAgent = headers.getRequestHeader("x-session").get(0);
        SessionChecker check=new SessionChecker();
        MessageWorker mWorker=new MessageWorker();
        String resp=check.Checker(userAgent);
        if(!resp.contains("Logout")){
            
        return mWorker.getNoticeString();
        }
        else
            return"Logout";
    }
    
    
    @POST
    @Consumes("application/json")
    @Produces("application/json")
    public String postJson(String content) {
        //TODO return proper representation object
        //throw new UnsupportedOperationException();
        
        return "Message Test";
    }
    
    @POST
    @Path("/idname/")
    @Consumes("application/json")
    @Produces("application/json")
    public String getDept(String content,@Context HttpHeaders headers) {
        //TODO return proper representation object
        //throw new UnsupportedOperationException();
        String userAgent = headers.getRequestHeader("x-session").get(0);
        SessionChecker check=new SessionChecker();
        MessageWorker mWorker=new MessageWorker();
        String resp=check.Checker(userAgent);
        if(!resp.contains("Logout")){
            
        return mWorker.getidname(content);
        }
        else
            return"Logout";

    }
    
    
    
     @POST
    @Path("/search/")
    @Consumes("application/json")
    @Produces("application/json")
    public String getSearch(String content,@Context HttpHeaders headers) {
        //TODO return proper representation object
        //throw new UnsupportedOperationException();
        String userAgent = headers.getRequestHeader("x-session").get(0);
        SessionChecker check=new SessionChecker();
        MessageWorker mWorker=new MessageWorker();
        String resp=check.Checker(userAgent);
        if(!resp.contains("Logout")){
            
        return mWorker.getSearch(content);
        }
        else
            return"Logout";

    }
    
    
    
    
    
     @POST
    @Path("/depnamefromid/")
    @Consumes("application/json")
    @Produces("application/json")
    public String getDeptName(String content,@Context HttpHeaders headers) throws JSONException {
        //TODO return proper representation object
        //throw new UnsupportedOperationException();
        String userAgent = headers.getRequestHeader("x-session").get(0);
        SessionChecker check=new SessionChecker();
        MessageWorker mWorker=new MessageWorker();
        String resp=check.Checker(userAgent);
        if(!resp.contains("Logout")){
            
        return mWorker.getDepName(content);
        }
        else
            return"Logout";

    }
    
    
  /*  
     @POST
    @Path("/namefromid/")
    @Consumes("application/json")
    @Produces("application/json")
    public String getName(String content,@Context HttpHeaders headers) {
        //TODO return proper representation object
        //throw new UnsupportedOperationException();
        String userAgent = headers.getRequestHeader("x-session").get(0);
        SessionChecker check=new SessionChecker();
        MessageWorker mWorker=new MessageWorker();
        String resp=check.Checker(userAgent);
        if(!resp.contains("Logout")){
            
        return mWorker.getidname(content);
        }
        else
            return"Logout";

    }
    */
    
    
    @POST
    @Path("/sendmessage/")
    @Consumes("application/json")
    @Produces("application/json")
    public String sndMessage(String content,@Context HttpHeaders headers) {
        //TODO return proper representation object
        //throw new UnsupportedOperationException();
        String userAgent = headers.getRequestHeader("x-session").get(0);
        SessionChecker check=new SessionChecker();
        MessageWorker mWorker=new MessageWorker();
        String resp=check.Checker(userAgent);
        if(!resp.contains("Logout")){
            
        return mWorker.sendMessage(content);
        }
        else
            return"Logout";

    }
    
    
    
    @POST
    @Path("/inbox/")
    @Consumes("application/json")
    @Produces("application/json")
    public String inbox(String content,@Context HttpHeaders headers) {
        //TODO return proper representation object
        //throw new UnsupportedOperationException();
        String userAgent = headers.getRequestHeader("x-session").get(0);
        SessionChecker check=new SessionChecker();
        MessageWorker mWorker=new MessageWorker();
        String resp=check.Checker(userAgent);
        if(!resp.contains("Logout")){
            
        return mWorker.inbox(content);
        }
        else
            return"Logout";

    }
    
     
    @POST
    @Path("/outbox/")
    @Consumes("application/json")
    @Produces("application/json")
    public String outbox(String content,@Context HttpHeaders headers) {
        //TODO return proper representation object
        //throw new UnsupportedOperationException();
        String userAgent = headers.getRequestHeader("x-session").get(0);
        SessionChecker check=new SessionChecker();
        MessageWorker mWorker=new MessageWorker();
        String resp=check.Checker(userAgent);
        if(!resp.contains("Logout")){
            
        return mWorker.outbox(content);
        }
        else
            return"Logout";

    }
    
    
    
    
 /*   
    
    	@POST
	@Path("/upload/")
	@Consumes(MediaType.MULTIPART_FORM_DATA)
	public Response uploadFile(
		@FormDataParam("file") InputStream uploadedInputStream,
		@FormDataParam("file") FormDataContentDisposition fileDetail) {
 
		String uploadedFileLocation = "d://uploaded/" + fileDetail.getFileName();
 
		// save it
		writeToFile(uploadedInputStream, uploadedFileLocation);
 
		String output = "File uploaded to : " + uploadedFileLocation;
 
		return Response.status(200).entity(output).build();
 
	}
 
	// save uploaded file to new location
	private void writeToFile(InputStream uploadedInputStream,
		String uploadedFileLocation) {
 
		try {
			OutputStream out = new FileOutputStream(new File(
					uploadedFileLocation));
			int read = 0;
			byte[] bytes = new byte[1024];
 
			out = new FileOutputStream(new File(uploadedFileLocation));
			while ((read = uploadedInputStream.read(bytes)) != -1) {
				out.write(bytes, 0, read);
			}
			out.flush();
			out.close();
		} catch (IOException e) {
 
			e.printStackTrace();
		}
 
	}
    
    
    
    
    
    */

    /**
     * PUT method for updating or creating an instance of MessageResource
     * @param content representation for the resource
     * @return an HTTP response with content of the updated or created resource.
     */
    @PUT
    @Consumes("application/json")
    public void putJson(String content) {
    }
}
