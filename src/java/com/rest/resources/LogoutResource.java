/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.rest.resources;

import com.rest.worker.LogoutDbWorker;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.UriInfo;
import javax.ws.rs.PathParam;
import javax.ws.rs.Consumes;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.GET;
import javax.ws.rs.Produces;
import javax.enterprise.context.RequestScoped;
import javax.ws.rs.POST;
import javax.ws.rs.core.HttpHeaders;

/**
 * REST Web Service
 *
 * @author phenom
 */
@Path("logout")
@RequestScoped
public class LogoutResource {

    @Context
    private UriInfo context;

    /**
     * Creates a new instance of LogoutResource
     */
    public LogoutResource() {
    }

    /**
     * Retrieves representation of an instance of com.rest.resources.LogoutResource
     * @return an instance of java.lang.String
     */
    @GET
    @Produces("application/json")
    public String getJson() {
        //TODO return proper representation object
        throw new UnsupportedOperationException();
    }

    /**
     * PUT method for updating or creating an instance of LogoutResource
     * @param content representation for the resource
     * @return an HTTP response with content of the updated or created resource.
     */
    
    @POST
    @Consumes("application/json")
    @Produces("application/json")
    public String postJson(String content, @Context HttpHeaders headers){
        LogoutDbWorker mDb=new LogoutDbWorker();
        String userAgent = headers.getRequestHeader("x-session").get(0);
        String res=mDb.logout(content,userAgent);
        return res;
    }
    
    @PUT
    @Consumes("application/json")
    public void putJson(String content) {
    }
}
