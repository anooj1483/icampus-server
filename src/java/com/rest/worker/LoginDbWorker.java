/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.rest.worker;

import com.connector.data.StudentData;
import com.connector.data.TstaffData;
import com.google.gson.Gson;
import com.rest.HibernateUtil;
import com.rest.TblDept;
import com.rest.TblDesignation;
import com.rest.TblHod;
import com.rest.TblLogin;
import com.rest.TblOnline;
import com.rest.TblStudent;
import com.rest.TblTstaff;
import com.rest.TblTutor;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.WindowConstants;
import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;
import org.hibernate.classic.Session;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONTokener;

/**
 *
 * @author miketelis
 */
public class LoginDbWorker {

    String u, p;

    public String getlogin() {
        List<TblLogin> actorList = null;

        try {
            Session session = HibernateUtil.getSessionFactory().getCurrentSession();
            org.hibernate.Transaction tx = session.beginTransaction();
            Query q = session.createQuery("from TblLogin ");
            actorList = (List<TblLogin>) q.list();
            String json = new Gson().toJson(actorList);
            
            
            

            return "ANOOJ ";

            //actorList = (List<tbl_login>) q.list();

        } catch (Exception e) {
            e.printStackTrace();
            return "CATCH " + e;
        }

        //return actorList;
    }

    public String inslogin(String content) {
        try {
            //Session session = HibernateUtil.getSessionFactory().openSession();

            /*String hql = "INSERT INTO TblLogin(10420007,'Ferose','f','qwe123',NOW(),NOW())";
             Query query = session.createQuery(hql);
             int result = query.executeUpdate();
             return "Rows affected: " + result;*/

            //session.getTransaction().begin();
            Session session = null;
            Transaction trans = null;
            SessionFactory sessFact = new Configuration().configure().buildSessionFactory();
            session = sessFact.openSession();
            trans = session.beginTransaction();

            TblLogin login = new TblLogin();

            JSONObject job = new JSONObject(content);

            login.setPassword((String) job.get("password"));
            login.setUsername((String) job.get("username"));
            login.setId((Integer) job.getInt("id"));
            System.out.print(login.getId());
            //login.createdAt=(Date) job.get("createdat");
            String cr_at = (String) job.get("createdat");
            login.setSessionid((String) job.getString("sessionid"));
            //login.lastLogin=(Date) job.get("lastlogin");
            String last = (String) job.get("lastlogin");

            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
            Date created = sdf.parse(cr_at);
            login.setCreatedAt(created);

            Date lastlog = sdf.parse(last);
            login.setLastLogin(lastlog);

            session.save(login);
            trans.commit();





            return "Success";
        } catch (JSONException ex) {
            return "Error " + ex;
            //Logger.getLogger(LoginDbWorker.class.getName()).log(Level.SEVERE, null, ex);
        } catch (ParseException ex) {
            //Logger.getLogger(LoginDbWorker.class.getName()).log(Level.SEVERE, null, ex);
            return "ERROR PRSE";
        }
    }

    public String checkLogin(String content) {
        try {

            
           
            
            
            
            Session session = null;
            Transaction trans = null;
            int dept_id = 0,desig_id = 0;
            String json = "JSON_ERROR";
            SessionFactory sessFact = new Configuration().configure().buildSessionFactory();
            session = sessFact.openSession();
            trans = session.beginTransaction();
            JSONObject job = new JSONObject(content);
            TstaffData staff=new TstaffData();
            StudentData student=new StudentData();
            TblTstaff login = new TblTstaff();
            List<TblTstaff> actorList = null;
            List<TblDept> DeptList = null;
            List<TblDesignation> DesigList = null;
            List<TblHod> hodList=null;
            List<TblTutor> tutorList=null;
            List<TblStudent> studentList=null;

            String type = job.getString("type");
            String username = job.getString("username");
            String password = job.getString("password");


            System.out.println(username);
            System.out.println(password);
            
            if (type.matches("HOD")) {
                 
                Query q1 = session.createQuery("from TblHod where staffId='" + username + "'");
                hodList = (List<TblHod>) q1.list();

                if(hodList.size()<=0){
                    return "Failed";
                }

                Query q = session.createQuery("from TblTstaff where staffId='" + username + "' and password='" + password + "'");
                actorList = (List<TblTstaff>) q.list();

                for (int i = 0; i < actorList.size(); i++) {

                    u = actorList.get(i).getStaffId();
                    p = actorList.get(i).getPassword();
                    staff.address=actorList.get(i).getAddress();
                    staff.blood=actorList.get(i).getBlood();
                    staff.contact=actorList.get(i).getContact();
                    dept_id=actorList.get(i).getTblDept().getDeptId();
                    desig_id=actorList.get(i).getTblDesignation().getDesigId();
                    staff.dob=actorList.get(i).getDob().toString();
                    staff.doj=actorList.get(i).getDoj().toString();
                    staff.gender=actorList.get(i).getGender();
                    staff.lastUpdated=actorList.get(i).getLastUpdated().toString();
                    staff.mail=actorList.get(i).getMail();
                    staff.staffId=actorList.get(i).getStaffId();
                    staff.prev1=actorList.get(i).getPrev1();
                    staff.prev2=actorList.get(i).getPrev2();
                    staff.qualif=actorList.get(i).getQualif();
                    staff.staffName=actorList.get(i).getStaffName();
                    staff.staffStatus=actorList.get(i).getStaffStatus();
                    staff.password=actorList.get(i).getPassword();
                    
                    
                }
                System.out.println("U; " + u);
                System.out.println("P; " + p);
                
                Query q2 = session.createQuery("from TblDept where deptId="+dept_id);
                DeptList = (List<TblDept>) q2.list();
                
                for (int i = 0; i < DeptList.size(); i++) {
                    staff.dept_name=DeptList.get(i).getDeptName();
   
                }
                
                Query q3 = session.createQuery("from TblDesignation where desigId="+desig_id);
                DesigList = (List<TblDesignation>) q3.list();
                
                for (int i = 0; i < DeptList.size(); i++) {
                    staff.desig_name=DesigList.get(i).getDesigName();
   
                }
                
                if (u.contains(username) && p.contains(password)) {
                    
                    
                    
                 json = new Gson().toJson(staff);
                
            }
            }
           else if (type.matches("Tutor")) {
                 
                Query q1 = session.createQuery("from TblTutor where staffId='" + username + "'");
                tutorList = (List<TblTutor>) q1.list();

                if(tutorList.size()<=0){
                    return "Failed";
                }

                Query q = session.createQuery("from TblTstaff where staffId='" + username + "' and password='" + password + "'");
                actorList = (List<TblTstaff>) q.list();

                for (int i = 0; i < actorList.size(); i++) {

                    u = actorList.get(i).getStaffId();
                    p = actorList.get(i).getPassword();
                    staff.address=actorList.get(i).getAddress();
                    staff.blood=actorList.get(i).getBlood();
                    staff.contact=actorList.get(i).getContact();
                    dept_id=actorList.get(i).getTblDept().getDeptId();
                    desig_id=actorList.get(i).getTblDesignation().getDesigId();
                    staff.dob=actorList.get(i).getDob().toString();
                    staff.doj=actorList.get(i).getDoj().toString();
                    staff.gender=actorList.get(i).getGender();
                    staff.lastUpdated=actorList.get(i).getLastUpdated().toString();
                    staff.mail=actorList.get(i).getMail();
                    staff.staffId=actorList.get(i).getStaffId();
                    staff.prev1=actorList.get(i).getPrev1();
                    staff.prev2=actorList.get(i).getPrev2();
                    staff.qualif=actorList.get(i).getQualif();
                    staff.staffName=actorList.get(i).getStaffName();
                    staff.staffStatus=actorList.get(i).getStaffStatus();
                    staff.password=actorList.get(i).getPassword();
                    
                    
                }
                System.out.println("U; " + u);
                System.out.println("P; " + p);
                
                Query q2 = session.createQuery("from TblDept where deptId="+dept_id);
                DeptList = (List<TblDept>) q2.list();
                
                for (int i = 0; i < DeptList.size(); i++) {
                    staff.dept_name=DeptList.get(i).getDeptName();
   
                }
                
                Query q3 = session.createQuery("from TblDesignation where desigId="+desig_id);
                DesigList = (List<TblDesignation>) q3.list();
                
                for (int i = 0; i < DeptList.size(); i++) {
                    staff.desig_name=DesigList.get(i).getDesigName();
   
                }
                
                if (u.contains(username) && p.contains(password)) {
                    
                    
                    
                 json = new Gson().toJson(staff);
                
            }
            }
           else if (type.matches("Teacher")) {
                 
                
                Query q = session.createQuery("from TblTstaff where staffId='" + username + "' and password='" + password + "'");
                actorList = (List<TblTstaff>) q.list();

                for (int i = 0; i < actorList.size(); i++) {

                    u = actorList.get(i).getStaffId();
                    p = actorList.get(i).getPassword();
                    staff.address=actorList.get(i).getAddress();
                    staff.blood=actorList.get(i).getBlood();
                    staff.contact=actorList.get(i).getContact();
                    dept_id=actorList.get(i).getTblDept().getDeptId();
                    desig_id=actorList.get(i).getTblDesignation().getDesigId();
                    staff.dob=actorList.get(i).getDob().toString();
                    staff.doj=actorList.get(i).getDoj().toString();
                    staff.gender=actorList.get(i).getGender();
                    staff.lastUpdated=actorList.get(i).getLastUpdated().toString();
                    staff.mail=actorList.get(i).getMail();
                    staff.staffId=actorList.get(i).getStaffId();
                    staff.prev1=actorList.get(i).getPrev1();
                    staff.prev2=actorList.get(i).getPrev2();
                    staff.qualif=actorList.get(i).getQualif();
                    staff.staffName=actorList.get(i).getStaffName();
                    staff.staffStatus=actorList.get(i).getStaffStatus();
                    staff.password=actorList.get(i).getPassword();
                    
                    
                }
                System.out.println("U; " + u);
                System.out.println("P; " + p);
                
                Query q2 = session.createQuery("from TblDept where deptId="+dept_id);
                DeptList = (List<TblDept>) q2.list();
                
                for (int i = 0; i < DeptList.size(); i++) {
                    staff.dept_name=DeptList.get(i).getDeptName();
   
                }
                
                Query q3 = session.createQuery("from TblDesignation where desigId="+desig_id);
                DesigList = (List<TblDesignation>) q3.list();
                
                for (int i = 0; i < DeptList.size(); i++) {
                    staff.desig_name=DesigList.get(i).getDesigName();
   
                }
                
                if (u.contains(username) && p.contains(password)) {
                    
                    
                    
                 json = new Gson().toJson(staff);
                
            }
            }
           else if (type.matches("Student")) {
                 
                

                Query q = session.createQuery("from TblStudent where studentId='" + username + "' and password='" + password + "'");
                studentList = (List<TblStudent>) q.list();

                for (int i = 0; i < studentList.size(); i++) {

                    u = studentList.get(i).getStudentId();
                    p = studentList.get(i).getPassword();
                    student.address=studentList.get(i).getAddress();
                    student.blood=studentList.get(i).getBlood();
                    //student.contact=actorList.get(i).getContact();
                    student.deptId=studentList.get(i).getDeptId();
                    dept_id=studentList.get(i).getDeptId();
                    student.semester=studentList.get(i).getSemester();
                   // staff.dob=actorList.get(i).getDob().toString();
                    //staff.doj=actorList.get(i).getDoj().toString();
                    student.gender=studentList.get(i).getGender();
                    //staff.lastUpdated=actorList.get(i).getLastUpdated().toString();
                    student.mail=studentList.get(i).getMail();
                    student.studentId=studentList.get(i).getStudentId();
                    //staff.prev1=actorList.get(i).getPrev1();
                    //staff.prev2=actorList.get(i).getPrev2();
                    //staff.qualif=actorList.get(i).getQualif();
                    student.studentName=studentList.get(i).getStudentName();
                    //staff.staffStatus=actorList.get(i).getStaffStatus();
                    student.password=studentList.get(i).getPassword();
                    
                    
                }
                System.out.println("U; " + u);
                System.out.println("P; " + p);
                
                Query q2 = session.createQuery("from TblDept where deptId="+dept_id);
                DeptList = (List<TblDept>) q2.list();
                
                for (int i = 0; i < DeptList.size(); i++) {
                    student.dept_name=DeptList.get(i).getDeptName();
   
                }
                
                
                
                if (u.contains(username) && p.contains(password)) {
                    
                    
                    
                 json = new Gson().toJson(student);
                
            }
            }
            
            
            if (u.contains(username) && p.contains(password)) {
                    
                
                return json;
            } else {
                return "Failed";
            }



            /*
             TblLogin login = new TblLogin();

             JSONObject job = new JSONObject(content);

             login.setPassword((String) job.get("password"));
             login.setUsername((String) job.get("username"));
             login.setId((Integer) job.getInt("type"));
             System.out.print(login.getId());
             //login.createdAt=(Date) job.get("createdat");
             String cr_at = (String) job.get("createdat");
             login.setSessionid((String) job.getString("sessionid"));
             //login.lastLogin=(Date) job.get("lastlogin");
             String last = (String) job.get("lastlogin");

             SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
             Date created = sdf.parse(cr_at);
             login.setCreatedAt(created);

             Date lastlog = sdf.parse(last);
             login.setLastLogin(lastlog);

             session.save(login);
             trans.commit();
             * */


            //return "Success";
        } catch (NullPointerException ex) {
            return "Failed ";
            //Logger.getLogger(LoginDbWorker.class.getName()).log(Level.SEVERE, null, ex);
        } catch (JSONException ex) {
            //Logger.getLogger(LoginDbWorker.class.getName()).log(Level.SEVERE, null, ex);
            return "Failed";
        }

    }
    
    public String SessionBuilder(String session_id,String checked,String content){
        try{
        Session session = null;
            Transaction trans = null;
            SessionFactory sessFact = new Configuration().configure().buildSessionFactory();
            session = sessFact.openSession();
            trans = session.beginTransaction();
            String Id="";
            

            JSONObject object = (JSONObject) new JSONTokener(checked).nextValue();
            JSONObject job = new JSONObject(content);
            String type = job.getString("type");
            if (type.contains("HOD") || type.contains("Teacher") || type.contains("Tutor")) {
                Id = object.getString("staffId");
            } else {
                Id = object.getString("studentId");
            }
            TblOnline online=new TblOnline();
            online.setId(Id);
            online.setSessionId("x-session: "+session_id);
            session.save(online);
            trans.commit();
            return "Success";
        }catch(Exception ex){
            return "Failed";
        }
    }
}
