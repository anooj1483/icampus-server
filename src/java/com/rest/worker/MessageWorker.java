/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.rest.worker;

import com.connector.data.InboxList;
import com.google.gson.Gson;
import com.rest.HibernateUtil;
import com.rest.TblDept;
import com.rest.TblHod;
import com.rest.TblHodinbox;
import com.rest.TblNotice;
import com.rest.TblStudent;
import com.rest.TblTinbox;
import com.rest.TblTstaff;
import com.rest.TblTutor;
import com.rest.TblTutorinbox;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.util.Date;
import java.util.List;
import java.util.Map;
import javax.json.Json;
import javax.json.JsonArrayBuilder;
import javax.json.JsonBuilderFactory;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.WindowConstants;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;
import org.hibernate.classic.Session;
import org.json.JSONException;
import org.json.simple.JSONArray;
import org.json.JSONObject;

/**
 *
 * @author phenom
 */
public class MessageWorker {

    public String getDepartment() {

        Session session = HibernateUtil.getSessionFactory().getCurrentSession();
        org.hibernate.Transaction tx = session.beginTransaction();
        List<TblDept> actorList = null;
        Query q = session.createQuery("Select deptName from TblDept");
        actorList = (List<TblDept>) q.list();
        for (int i = 0; i < actorList.size(); i++) {
        }
        String json = new Gson().toJson(actorList);
        System.out.print(json);
        return json;
    }

    public String getNoticeString() throws JSONException {

        Session session = HibernateUtil.getSessionFactory().getCurrentSession();
        org.hibernate.Transaction tx = session.beginTransaction();
        List<TblNotice> actorList = null;
        JSONObject mJson = new JSONObject();
        Query q = session.createQuery(" from TblNotice");
        actorList = (List<TblNotice>) q.list();
        for (int i = 0; i < actorList.size(); i++) {
            mJson.put("Message" + i, actorList.get(i).getMessage());
        }

        return mJson.toString();
    }

    public String getDepName(String content) throws JSONException {

        Session session = HibernateUtil.getSessionFactory().getCurrentSession();
        org.hibernate.Transaction tx = session.beginTransaction();
        List<TblDept> actorList = null;
        String departmnt = null;
        JSONObject mJson = new JSONObject();
        JSONObject job = new JSONObject(content);
        String depId = job.getString("Id");
        Query q = session.createQuery("from TblDept where deptId=" + depId);
        actorList = (List<TblDept>) q.list();
        for (int i = 0; i < actorList.size(); i++) {
            departmnt = actorList.get(i).getDeptName();
        }
        mJson.put("DeptName", departmnt);
        System.out.print(mJson.toString());
        return mJson.toString();
    }

    public String getidname(String content) {

        try {

            Session session = HibernateUtil.getSessionFactory().getCurrentSession();
            org.hibernate.Transaction tx = session.beginTransaction();
            tx.begin();
            List<TblDept> actorList1 = null;
            List<TblHod> actorList2 = null;
            List<TblTutor> actorList4 = null;
            List<TblTstaff> actorList3 = null;
            String query1 = "", query2 = "", query3 = "", staffId = "", staffName = "";
            int depId = 0;
            JSONObject mJson = new JSONObject();

            
            /**********************************************/
            
            /***********************************************/
            
            
            
            JSONObject job = new JSONObject(content);
            String from = job.getString("From");
            String to = job.getString("To");
            String to_dept = job.getString("To_dept");
            System.out.print("JSON OUT: " + from + " " + to + " " + to_dept);

            if (to.contains("HOD")) {
                query1 = "from TblDept where deptName='" + to_dept + "'";
                System.out.print(query1);

                Query q1 = session.createQuery(query1);
                //q.executeUpdate();

                actorList1 = (List<TblDept>) q1.list();

                for (int i = 0; i < actorList1.size(); i++) {
                    depId = actorList1.get(i).getDeptId();
                    System.out.print("JSON OUTq1: " + depId);
                }

                //depId=1;
                System.out.print("JSON OUTq1: " + depId);

                query2 = "from TblHod where deptId=" + depId;
                Query q2 = session.createQuery(query2);
                actorList2 = (List<TblHod>) q2.list();
                for (int i = 0; i < actorList2.size(); i++) {
                    staffId = actorList2.get(i).getStaffId();
                }
                System.out.print("JSON OUTq2: " + staffId);

                query3 = "from TblTstaff where staffId='" + staffId + "'";
                Query q3 = session.createQuery(query3);
                actorList3 = (List<TblTstaff>) q3.list();
                for (int i = 0; i < actorList3.size(); i++) {
                    staffName = actorList3.get(i).getStaffName();
                }
                System.out.print("JSON OUTq3: " + staffName);
                mJson.put("HodName", staffName);
            } else if (to.contains("Tutor")) {
                query1 = "from TblDept where deptName='" + to_dept + "'";
                System.out.print(query1);

                Query q1 = session.createQuery(query1);
                //q.executeUpdate();

                actorList1 = (List<TblDept>) q1.list();

                for (int i = 0; i < actorList1.size(); i++) {
                    depId = actorList1.get(i).getDeptId();
                    System.out.print("JSON OUTq1: " + depId);
                }

                //depId=1;
                System.out.print("JSON OUTq1: " + depId);

                query2 = "from TblTutor where deptId=" + depId;
                Query q2 = session.createQuery(query2);
                actorList4 = (List<TblTutor>) q2.list();
                for (int i = 0; i < actorList4.size(); i++) {
                    staffId = actorList4.get(i).getStaffId();
                }
                System.out.print("JSON OUTq2: " + staffId);

                query3 = "from TblTstaff where staffId='" + staffId + "'";
                Query q3 = session.createQuery(query3);
                actorList3 = (List<TblTstaff>) q3.list();
                for (int i = 0; i < actorList3.size(); i++) {
                    staffName = actorList3.get(i).getStaffName();
                }
                System.out.print("JSON OUTq3: " + staffName);
                mJson.put("HodName", staffName);

            } else if (to.contains("Teacher")) {
                query1 = "from TblDept where deptName='" + to_dept + "'";
                System.out.print(query1);

                Query q1 = session.createQuery(query1);
                //q.executeUpdate();

                actorList1 = (List<TblDept>) q1.list();

                for (int i = 0; i < actorList1.size(); i++) {
                    depId = actorList1.get(i).getDeptId();
                    System.out.print("JSON OUTq1: " + depId);
                }


                query3 = "from TblTstaff where tblDept=" + depId;
                Query q3 = session.createQuery(query3);
                actorList3 = (List<TblTstaff>) q3.list();

                for (int i = 0; i < actorList3.size(); i++) {
                    staffName = actorList3.get(i).getStaffName();

                    mJson.put("GetName" + i, staffName);
                }
                System.out.print("JSON OUTq3: " + staffName);

            }



            return mJson.toString();
        } catch (Exception e) {

            return "Failed" + e;

        }
    }

    public String inbox(String content) {
        try {
            Session session = HibernateUtil.getSessionFactory().getCurrentSession();
            org.hibernate.Transaction tx = session.beginTransaction();
            tx.begin();

            List<TblHodinbox> actorList1 = null;
            JSONObject job = new JSONObject(content);
            String id = job.getString("Id");
            String type = job.getString("Type");
            String query = "";


            TblHodinbox inb = new TblHodinbox();



            JSONArray inbo = new JSONArray();


            if (type.contains("HOD")) {

                query = "from TblHodinbox where staffId='" + id + "'";
                System.out.print(query);

                Query q1 = session.createQuery(query);
                //q.executeUpdate();

                actorList1 = (List<TblHodinbox>) q1.list();

                JSONObject mObject = new JSONObject();
                String from[] = new String[actorList1.size()];
                String subject[] = new String[actorList1.size()];
                String msg[] = new String[actorList1.size()];

                for (int i = 0; i < actorList1.size(); i++) {
                    from[i] = actorList1.get(i).getFromId();
                    subject[i] = actorList1.get(i).getSubject();
                    msg[i] = actorList1.get(i).getMsg();

                    System.out.print("\n" + from[i] + subject[i] + msg[i]);
                    mObject.put("From" + i, from[i]);
                    mObject.put("FromName" + i, getStaffName(from[i]));
                    mObject.put("Subject" + i, subject[i]);
                    mObject.put("Message" + i, msg[i]);
                    //inbo.add(mObject);
                    //System.out.print(mObject.toString());

                }

                mObject.put("Length", actorList1.size());
                System.out.print(mObject.toString());


                return mObject.toString();

            }
            /*else
             return "Failed";*/
            return "Success";
        } catch (HibernateException | JSONException e) {
            return "Failed" + e;
        }
    }

    public String outbox(String content) {
        try {
            Session session = HibernateUtil.getSessionFactory().getCurrentSession();
            org.hibernate.Transaction tx = session.beginTransaction();
            tx.begin();

            List<TblHodinbox> actorList1 = null;
            JSONObject job = new JSONObject(content);
            String id = job.getString("Id");
            String type = job.getString("Type");
            String query = "";


            TblHodinbox inb = new TblHodinbox();



            JSONArray inbo = new JSONArray();


            if (type.contains("HOD")) {

                query = "from TblHodinbox where fromId='" + id + "'";
                System.out.print(query);

                Query q1 = session.createQuery(query);
                //q.executeUpdate();

                actorList1 = (List<TblHodinbox>) q1.list();

                JSONObject mObject = new JSONObject();
                String from[] = new String[actorList1.size()];
                String subject[] = new String[actorList1.size()];
                String msg[] = new String[actorList1.size()];

                for (int i = 0; i < actorList1.size(); i++) {
                    from[i] = actorList1.get(i).getStaffId();
                    subject[i] = actorList1.get(i).getSubject();
                    msg[i] = actorList1.get(i).getMsg();

                    System.out.print("\n" + from[i] + subject[i] + msg[i]);
                    mObject.put("From" + i, from[i]);
                    mObject.put("FromName" + i, getStaffName(from[i]));
                    mObject.put("Subject" + i, subject[i]);
                    mObject.put("Message" + i, msg[i]);
                    //inbo.add(mObject);
                    //System.out.print(mObject.toString());

                }

                mObject.put("Length", actorList1.size());
                System.out.print(mObject.toString());


                return mObject.toString();

            }
            /*else
             return "Failed";*/
            return "Success";
        } catch (HibernateException | JSONException e) {
            return "Failed" + e;
        }
    }

    public String getSearch(String content) {
        try {
            Session session = HibernateUtil.getSessionFactory().getCurrentSession();
            org.hibernate.Transaction tx = session.beginTransaction();
            tx.begin();
            List<TblTstaff> staffList = null;
            List<TblStudent> studentList=null;
            JSONObject mJson = new JSONObject(content);
            String id = mJson.getString("Id");
            String type = "";
            String name = "";
            int deptid = 0;
            String dept = "";

            String query = "from TblTstaff where staffId='" + id + "'";
            System.out.print(query);

            Query q1 = session.createQuery(query);
            staffList = (List<TblTstaff>) q1.list();
            
            
            String query2 = "from TblStudent where studentId='" + id + "'";
            System.out.print(query2);

            Query q2 = session.createQuery(query2);
            studentList = (List<TblStudent>) q2.list();

            if (staffList.size() > 0) {

                for (int i = 0; i < staffList.size(); i++) {
                    name = staffList.get(i).getStaffName();
                    deptid = staffList.get(i).getTblDept().getDeptId();
                }
                JSONObject ob = new JSONObject();
                ob.put("Id", deptid);
                String resp = getDepName(ob.toString());
                JSONObject ob1 = new JSONObject(resp);
                dept = ob1.getString("DeptName");
                mJson.put("Name", name);
                mJson.put("Dept", dept);
                mJson.put("Type", "Staff");
            }
            else if(studentList.size()>0)
            {
                
                for (int i = 0; i < studentList.size(); i++) {
                    name = studentList.get(i).getStudentName();
                    deptid = studentList.get(i).getDeptId();
                }
                JSONObject ob = new JSONObject();
                ob.put("Id", deptid);
                String resp = getDepName(ob.toString());
                JSONObject ob1 = new JSONObject(resp);
                dept = ob1.getString("DeptName");
                mJson.put("Name", name);
                mJson.put("Dept", dept);
                mJson.put("Type", "Student");
            }
            else
            {
                
                mJson.put("Name", "Name Not Found");
                mJson.put("Dept", "Department Not Found");
                mJson.put("Type", "Type Not Found");
            }
             System.out.println(mJson.toString());
            return mJson.toString();
        } catch (Exception e) {
            return "NoID";

        }
    }

    public String getStaffName(String id) {
        try {
            Session session = HibernateUtil.getSessionFactory().getCurrentSession();
            org.hibernate.Transaction tx = session.beginTransaction();
            tx.begin();
            List<TblTstaff> actorList1 = null;
            String name = "";
            String query = "from TblTstaff where staffId='" + id + "'";
            System.out.print(query);

            Query q1 = session.createQuery(query);
            actorList1 = (List<TblTstaff>) q1.list();

            for (int i = 0; i < actorList1.size(); i++) {
                name = actorList1.get(i).getStaffName();
            }

            return name;
        } catch (Exception e) {
            return "NoID";

        }
    }

    public String sendMessage(String content) {

        try {

            Session session = null;
            Transaction trans = null;
            SessionFactory sessFact = new Configuration().configure().buildSessionFactory();
            session = sessFact.openSession();
            trans = session.beginTransaction();

            List<TblDept> actorList1 = null;
            List<TblHod> actorList2 = null;
            List<TblHodinbox> actorList3 = null;
            List<TblTutorinbox> actorList5 = null;
            List<TblTinbox> teacherInbox = null;
            List<TblTstaff> staffList = null;
            List<TblTutor> actorList4 = null;
            String query1 = "", query2 = "", query3 = "", staffId = "", staffName = "";
            int depId = 0, size = 0;

            JSONObject job = new JSONObject(content);
            String from = job.getString("From");
            String to = job.getString("To");
            String dept = job.getString("Department");
            String message = job.getString("Message");
            String subject = job.getString("Subject");
            String id = job.getString("Id");
            String fromid = job.getString("FromId");


            System.out.print("JSON OUT: " + from + " " + to + " " + dept + " " + message + " " + subject + " " + id);

            if (to.contains("HOD")) {
                query1 = "from TblDept where deptName='" + dept + "'";
                System.out.print(query1);

                Query q1 = session.createQuery(query1);
                //q.executeUpdate();

                actorList1 = (List<TblDept>) q1.list();

                for (int i = 0; i < actorList1.size(); i++) {
                    depId = actorList1.get(i).getDeptId();
                    System.out.print("getMsgq1: " + depId);
                }

                query2 = "from TblHod where deptId=" + depId;
                Query q2 = session.createQuery(query2);
                actorList2 = (List<TblHod>) q2.list();
                for (int i = 0; i < actorList2.size(); i++) {
                    staffId = actorList2.get(i).getStaffId();
                }
                System.out.print("getmsgq2: " + staffId);


                query2 = "from TblHodinbox";
                Query q3 = session.createQuery(query2);
                actorList3 = (List<TblHodinbox>) q3.list();
                size = actorList3.size();


                java.util.Date d = new java.util.Date();
                int day = d.getDate();
                int month = d.getMonth();
                int year = d.getYear();
                String date = year + "-" + month + "-" + day;




                TblHodinbox inbox = new TblHodinbox();
                inbox.setStaffId(staffId);
                inbox.setAttachment("NIL");
                inbox.setFromId(fromid);
                inbox.setIsRead(0);
                inbox.setMsgId(size++);
                inbox.setMsg(message);
                inbox.setRcvDateTime(new java.sql.Date(d.getTime()));
                inbox.setSubject(subject);
                session.save(inbox);
                trans.commit();

                return "Success";
            } else if (to.contains("Tutor")) {

                query1 = "from TblDept where deptName='" + dept + "'";
                System.out.print(query1);

                Query q1 = session.createQuery(query1);
                //q.executeUpdate();

                actorList1 = (List<TblDept>) q1.list();

                for (int i = 0; i < actorList1.size(); i++) {
                    depId = actorList1.get(i).getDeptId();
                    System.out.print("getMsgq1: " + depId);
                }

                query2 = "from TblTutor where deptId=" + depId;
                Query q2 = session.createQuery(query2);
                actorList4 = (List<TblTutor>) q2.list();
                for (int i = 0; i < actorList4.size(); i++) {
                    staffId = actorList4.get(i).getStaffId();
                }
                System.out.print("getmsgq2: " + staffId);


                query2 = "from TblTutorinbox";
                Query q3 = session.createQuery(query2);
                actorList5 = (List<TblTutorinbox>) q3.list();
                size = actorList5.size();


                java.util.Date d = new java.util.Date();
                int day = d.getDate();
                int month = d.getMonth();
                int year = d.getYear();
                String date = year + "-" + month + "-" + day;




                TblTutorinbox inbox = new TblTutorinbox();
                inbox.setStaffId(staffId);
                inbox.setAttachment("NIL");
                inbox.setFromId(fromid);
                inbox.setIsRead(0);
                inbox.setMsgId(size++);
                inbox.setMsg(message);
                inbox.setRcvDateTime(new java.sql.Date(d.getTime()));
                inbox.setSubject(subject);
                session.save(inbox);
                trans.commit();

                return "Success";
            } else if (to.contains("Teacher")) {

                query1 = "from TblDept where deptName='" + dept + "'";
                System.out.print(query1);

                Query q1 = session.createQuery(query1);
                //q.executeUpdate();

                actorList1 = (List<TblDept>) q1.list();

                for (int i = 0; i < actorList1.size(); i++) {
                    depId = actorList1.get(i).getDeptId();
                    System.out.print("getMsgq1: " + depId);
                }

                query2 = "from TblTstaff where deptId=" + depId;
                Query q2 = session.createQuery(query2);
                staffList = (List<TblTstaff>) q2.list();
                for (int i = 0; i < staffList.size(); i++) {
                    staffId = staffList.get(i).getStaffId();
                }
                System.out.print("getmsgq2: " + staffId);


                query2 = "from TblTinbox";
                Query q3 = session.createQuery(query2);
                teacherInbox = (List<TblTinbox>) q3.list();
                size = teacherInbox.size();


                java.util.Date d = new java.util.Date();
                int day = d.getDate();
                int month = d.getMonth();
                int year = d.getYear();
                String date = year + "-" + month + "-" + day;




                TblTinbox inbox = new TblTinbox();
                inbox.setStaffId(staffId);
                inbox.setAttachment("NIL");
                inbox.setFromId(fromid);
                inbox.setIsRead(0);
                inbox.setMsgId(size++);
                inbox.setMsg(message);
                inbox.setRcvDateTime(new java.sql.Date(d.getTime()));
                inbox.setSubject(subject);
                session.save(inbox);
                trans.commit();

                return "Success";
            } else {
                return "Success";
            }
        } catch (Exception ex) {
            return "Failed" + ex;
        }
    }
}
