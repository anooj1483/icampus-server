/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.rest.worker;

import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;
import org.hibernate.classic.Session;
import org.json.JSONObject;

/**
 *
 * @author phenom
 */
public class LogoutDbWorker {
    
    public String logout(String content,String session_id){
        
        try{
            Session session = null;
            Transaction trans = null;
            SessionFactory sessFact = new Configuration().configure().buildSessionFactory();
            session = sessFact.openSession();
            trans = session.beginTransaction();
            JSONObject job = new JSONObject(content);
             String id = job.getString("id");
             
             Query q=session.createQuery("delete from TblOnline where id='"+id+"'and session_id='"+session_id+"'");
             int result=q.executeUpdate();
             if(result==1)
            return "Success";
             else
                 return "Failed";
        }
        catch(Exception ex)
        {
        return "Failed";
        }
    }
    
    
    public String logoutwithid(String content){
        
        try{
            Session session = null;
            Transaction trans = null;
            SessionFactory sessFact = new Configuration().configure().buildSessionFactory();
            session = sessFact.openSession();
            trans = session.beginTransaction();
            JSONObject job = new JSONObject(content);
             String id = job.getString("username");
             
             Query q=session.createQuery("delete from TblOnline where id='"+id+"'");
             int result=q.executeUpdate();
             if(result==1)
            return "Success";
             else
                 return "Failed";
        }
        catch(Exception ex)
        {
        return "Failed";
        }
    }
    
    
}
