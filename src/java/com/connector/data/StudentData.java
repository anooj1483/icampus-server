/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.connector.data;

import java.util.Date;

/**
 *
 * @author root
 */
public class StudentData {
    
    public String studentId;
    public String dept_name;
     public String studentName;
     public String address;
     public String mail;
     public String parantId;
     public Date dob;
     public String gender;
     public String accomodation;
     public String hostelAddress;
     public String reservation;
     public String religion;
     public String cast;
     public int feeConcession;
     public String quota;
     public int deptId;
     public int semester;
     public String password;
     public String studentCode;
     public String plusTwo;
     public String tenth;
     public Date doj;
     public String blood;
     public Date lastUpdated;
     public int accStatus;
    
}
