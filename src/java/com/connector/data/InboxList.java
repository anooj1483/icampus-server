/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.connector.data;

/**
 *
 * @author phenom
 */
public class InboxList {

     public String fromId;
     public String subject;
     public String msg;

    public InboxList(String fromId, String subject, String msg) {
        this.fromId = fromId;
        this.subject = subject;
        this.msg = msg;
    }

    public InboxList() {
        //throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public String getFromId() {
        return fromId;
    }

    public void setFromId(String fromId) {
        this.fromId = fromId;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }
    
}
