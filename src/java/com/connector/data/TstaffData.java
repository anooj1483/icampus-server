/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.connector.data;

import com.rest.TblDept;
import com.rest.TblDesignation;
import java.util.Date;

/**
 *
 * @author phenom
 */
public class TstaffData {
    
    public String staffId;
     public int deptid;
     public int  designation;
     public String staffName;
     public String qualif;
     public String doj;
     public String prev1;
     public String prev2;
     public String gender;
     public String contact;
     public String mail;
     public String address;
     public String dob;
     public String blood;
     public String lastUpdated;
     public String accStatus;
     public String password;
     public String staffStatus;
     public String desig_name;
     public String dept_name;
    
}
