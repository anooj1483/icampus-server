/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.connector.data;

import java.util.Date;

/**
 *
 * @author phenom
 */
public class LoginData {
    
    public int id;
    public String username = null;
    public String password = null;
    public String sessionid = null;
    public Date createdAt;
    public Date lastLogin;
}
